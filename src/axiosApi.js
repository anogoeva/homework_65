import axios from 'axios';

const axiosApi = axios.create({
    baseURL: 'https://homework-65-685b9-default-rtdb.firebaseio.com/pages'
});

export default axiosApi;