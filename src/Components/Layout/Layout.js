import React from 'react';
import './Layout.css'
import Toolbar from "../Navigation/Toolbar/Toolbar";

const Layout = ({children}) => {
    return (
        <div className="container">
            <Toolbar/>
            <main className="Layout-Content">{children}</main>
        </div>
    );
};

export default Layout;