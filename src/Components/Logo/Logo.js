import React from 'react';
import './Logo.css';
import images from '../../assets/images/images (1).png'

const Logo = () => {
    return (
        <div className="Logo">
            <img src={images} alt="BurgerApp"/>
        </div>
    );
};

export default Logo;