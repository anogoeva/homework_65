import React from 'react';
import './NavigationItems.css'
import NavigationItem from "./NavigationItem/NavigationItem";

const NavigationItems = () => {
    return (
        <ul className="NavigationItem">
            <NavigationItem to="/" exact>Home</NavigationItem>
            <NavigationItem to="/pages/about">About</NavigationItem>
            <NavigationItem to="/pages/contacts">Contacts</NavigationItem>
            <NavigationItem to="/pages/divisions">Divisions</NavigationItem>
            <NavigationItem to="/pages/admin">Admin</NavigationItem>
        </ul>
    );
};

export default NavigationItems;