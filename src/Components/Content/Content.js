import React, {useEffect, useState} from 'react';
import axiosApi from "../../axiosApi";

const Content = ({match}) => {
    let params = match.params.page;

    if (!params) {
        params = 'home';
    }

    const [content, setContent] = useState({});
    useEffect(() => {
        const fetchData = async () => {
            const response = await axiosApi.get('/' + params + '.json');
            setContent(response.data);
        };
        fetchData().catch(console.error);
    }, [params]);

    const img = content.img ? <img src={content.img} alt="" width="200" height="200"/> : null;

    return (
        <div>
            <div>
                <h1>{content.title}</h1>
                <p>{content.content}</p>
                {img}
            </div>
        </div>
    );
};

export default Content;