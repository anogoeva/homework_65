import React from 'react';
import {BrowserRouter, Route, Switch} from "react-router-dom";
import Layout from "./Components/Layout/Layout";
import Content from "./Components/Content/Content";
import EditPage from "./Containers/EditPage/EditPage";

const App = () => (
    <BrowserRouter>
        <Layout>
            <Switch>
                <Route path="/" exact component={Content}/>
                <Route path="/pages/admin" component={EditPage}/>
                <Route path="/pages/:page" component={Content}/>
            </Switch>
        </Layout>
    </BrowserRouter>
);

export default App;
